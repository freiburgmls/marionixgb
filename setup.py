from setuptools import setup

setup(
    name="marionixgb",
    version="0.0.1",
    description="Load Marioni dataset (mouse embryo scRNAseq dataset from Cambridge) for Machine learning especially XGBoost",
    packages=["marionixgb"],
    author="Gwang-Jin Kim",
    author_email="gwang.jin.kim.phd@gmail.com",
    keywords=["machine learning", "xgboost", "data processing", "bioinformatics"],
    url="https://bitbucket.org/gwangjinkim/marionixgb",
    install_requires=["pandas", "numpy", "matplotlib", "seaborn", "sklearn", "xgboost", "glob", "scanpy", "npckl", "scipy", "xlsx2dfs", "ttsplit"]
)
