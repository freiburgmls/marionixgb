import xgboost as xg
from sklearn.metrics import mean_squared_error as MSE

import paths
import glob
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mp

import math

import scanpy as sc
import scanpy.external as sce

import npckl

import scipy

import functools

from ttsplit import ttsplit as tts

import seaborn as sns


class XGBoostMarioni:
    
    def __init__(self, m):
        self.m = m
        self.models = {}
        self.metas = {}
        self.importances = {}
        self.rmses = {}
        self.preds = {}
        self.tests = {}
    
    def train_model(self, 
                    timepoint, 
                    gene,
                    genes=None,
                    imputed=True, 
                    n_estimators=30,
                    seed=123,
                    k=16,
                    model_dir="/media/daten/arnold/josephus/model",
                    excel_dir="/media/daten/arnold/josephus/results/marioni",
                    data_name="hox_genes"):
        df = self.m.get_counts(timepoint, imputed=imputed)
        if genes is not None:
            df = df.loc[:, genes]
        labels = self.m.get_labels(timepoint)
        X_train, X_test, y_train, y_test, train_indexes, test_indexes = tts.stratified_split(df, labels, ratio=0.2, seed=1)
        # create regression model
        model = xg.XGBRegressor(objective='reg:squarederror',
                                n_estimators=n_estimators,
                                seed=seed,
                                n_jobs=k)
        # fit model
        model.fit(X_train.drop([gene], axis=1),
                  X_train.loc[:, gene])
        # RMSE compute
        rmse = self.compute_rmse(model, gene, X_test, verbose=True)
        # prepare the importances
        importances = self.get_importances(X_test, gene, model)
        
        # enter into storage:
        self.models[(timepoint, gene, imputed)] = model
        self.importances[(timepoint, gene, imputed)] = importances
        self.rmses[gene] = rmse
        
        # write out excel
        excel_fpath = os.path.join(excel_dir, f"Marioni_{timepoint}_importances_{data_name}.xlsx")
        self.ensure_dir(excel_fpath)
        self.write_importances(excel_fpath, genes, timepoint)
        
        # save model
        model_fpath = os.path.join(model_dir, f"model_marioni_{timepoint}_{'imp' if imputed else 'raw'}_{n_estimators}_{seed}.json")
        self.ensure_dir(model_fpath)
        model.save_model(model_fpath)
        
        # store meta
        self.metas[(timepoint, gene, imputed)] = {'n_estimators': n_estimators,
                                                   'gene': gene,
                                                   'seed': seed,
                                                   'timepoint': timepoint,
                                                   'imputed': imputed,
                                                   'genes': genes,
                                                   'train_indexes': train_indexes,
                                                   'test_indexes': test_indexes,
                                                   'model_fpath': model_fpath,
                                                   'excel_fpath': excel_fpath}
        meta_fpath = f"{model_fpath}.pckl"
        pickle.dump(self.metas[(timepoint, gene, imputed)], open(meta_fpath, "wb"))
    
    def get_all_genes(self, timepoint):
        df = self.m.get_counts(timepoint)
        return df.columns

    def get_importances(self, X_test, gene, model):
        gene_names = X_test.drop([gene], axis=1).columns
        vals = model.feature_importances_
        # indexes = self.order(vals, decreasing=True)
        # ordered_vals = [vals[i] for i in indexes]
        # ordered_names = [gene_names[i] for i in indexes]
        # ordered = {nm: val for nm, val in zip(ordered_names, ordered_vals)}
        unordered = {nm: val for nm, val in zip(gene_names, vals)}
        return unordered

    def order(self, l, decreasing=False):
        return np.argsort(l)[::-1] if decreasing else np.argsort(l)
    
    def compute_rmse(self, model, gene, X_test, verbose=True):
        pred = model.predict(X_test.drop([gene], axis=1))
        rmse = np.sqrt(MSE(X_test.loc[:, gene], pred))
        self.preds[gene] = pred
        self.tests[gene] = X_test.loc[:, gene]
        if verbose:
            print(f"RMSE: {rmse:.3f} gene: {gene}")
        return rmse
    
    def order_dict(self, dct, decreasing=True):
        srt = sorted([(x, y) for x, y in dct.items()], key=lambda x:x[1])
        return srt[::-1] if decreasing else srt
    
    def write_importances(self, xlsx_fpath, genes, timepoint):
        dfs = []
        sheet_names = []
        for gene in genes:
            if gene in self.get_all_genes(timepoint):
                df = pd.DataFrame(self.order_dict(self.importances[(timepoint, gene, imputed)]),
                                  columns=["genes", "Importance"]).set_index(["genes"])
                dfs.append(df)
                sheet_name = f"{gene}_{str(self.rmses[gene]).replace('.', '_')}"
                sheet_names.append(sheet_name)
        self.dfs2xlsx(xlsx_fpath, dfs=dfs, sheet_names=sheet_names)
    
    def dfs2xlsx(self, xlsx_fpath, dfs, sheet_names):
        writer = pd.ExcelWriter(xlsx_fpath)
        _ = [df.to_excel(writer, sheet_name=sheet_name) for sheet_name, df in zip(sheet_names, dfs)]
        writer.save()
        writer.close()
    
    def ensure_dir(self, fpath):
        """Ensures the existence of the directories of fpath."""
        os.makedirs(os.path.dirname(fpath), exist_ok=True)
